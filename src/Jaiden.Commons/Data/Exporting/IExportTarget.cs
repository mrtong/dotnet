﻿using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public interface IExportTarget
    {
        void Export<T>(IEnumerable<T> data, IExportRole<T> role);
    }
}