﻿using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public abstract class ExportRoleBase<T> : IExportRole<T>
    {
        private readonly ExportColumnBuilder<T> _builder;

        protected ExportRoleBase()
        {
            _builder = new ExportColumnBuilder<T>();
            Initialize();
        }

        public abstract string Name { get; }
        public IReadOnlyList<ExportColumn<T>> Columns => _builder.Columns;

        private void Initialize()
        {
            Initialize(_builder);
        }

        protected abstract void Initialize(ExportColumnBuilder<T> builder);
    }
}