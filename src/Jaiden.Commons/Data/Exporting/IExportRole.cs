﻿using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public interface IExportRole<T>
    {
        string Name { get; }
        IReadOnlyList<ExportColumn<T>> Columns { get; }
    }
}