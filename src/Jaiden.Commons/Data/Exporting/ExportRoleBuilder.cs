﻿using System;
using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public class ExportRoleBuilder<T>
    {
        private readonly ExportColumnBuilder<T> _columnBuilder;
        private readonly InnerExportRole _role;

        public ExportRoleBuilder(string name)
        {
            Name = name;
            _columnBuilder = new ExportColumnBuilder<T>();
            _role = new InnerExportRole(this);
        }

        public string Name { get; set; }
        public IExportRole<T> ExportRole => _role;

        public void AddColumn<TValue>(string name, Func<T, TValue> getter)
        {
            _columnBuilder.AddColumn(name, getter);
        }

        private class InnerExportRole : IExportRole<T>
        {
            private readonly ExportRoleBuilder<T> _builder;

            public InnerExportRole(ExportRoleBuilder<T> builder)
            {
                _builder = builder;
            }

            public string Name => _builder.Name;
            public IReadOnlyList<ExportColumn<T>> Columns => _builder._columnBuilder.Columns;
        }
    }
}