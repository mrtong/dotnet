﻿using System;

namespace Jaiden.Data.Exporting
{
    public class ExportColumn<T>
    {
        internal ExportColumn()
        {
        }

        public string Name { get; internal set; }
        public Func<T, object> ValueGetter { get; internal set; }
        public Type ValueType { get; internal set; }
    }
}