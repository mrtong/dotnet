﻿using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public static class ExportRoleExtensions
    {
        public static void Export<T>(this IExportRole<T> role, IEnumerable<T> data, IExportTarget target)
        {
            target.Export(data, role);
        }
    }
}