﻿using System;
using System.Collections.Generic;

namespace Jaiden.Data.Exporting
{
    public class ExportColumnBuilder<T>
    {
        private readonly List<ExportColumn<T>> _columns;

        public ExportColumnBuilder()
        {
            _columns = new List<ExportColumn<T>>();
        }

        public IReadOnlyList<ExportColumn<T>> Columns => _columns;

        public void AddColumn<TValue>(string name, Func<T, TValue> getter)
        {
            var column = new ExportColumn<T>
            {
                Name = name,
                ValueGetter = i => getter(i),
                ValueType = typeof(TValue)
            };
            _columns.Add(column);
        }
    }
}