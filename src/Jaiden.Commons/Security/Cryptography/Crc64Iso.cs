﻿namespace Jaiden.Security.Cryptography
{
    public class CRC64Iso : CRC64
    {
        public const ulong Iso3309Polynomial = 0xD800000000000000;
        internal static ulong[] Table;

        public CRC64Iso()
            : base(Iso3309Polynomial)
        {
        }

        public CRC64Iso(ulong seed)
            : base(Iso3309Polynomial, seed)
        {
        }

        public static ulong Compute(byte[] buffer)
        {
            return Compute(DefaultSeed, buffer);
        }

        public static ulong Compute(ulong seed, byte[] buffer)
        {
            if (Table == null)
            {
                Table = CreateTable(Iso3309Polynomial);
            }

            return CalculateHash(seed, Table, buffer, 0, buffer.Length);
        }
    }
}