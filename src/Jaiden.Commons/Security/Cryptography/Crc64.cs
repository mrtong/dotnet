﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Jaiden.Security.Cryptography
{
    /// <summary>
    ///     Implements a 64-bit CRC hash algorithm for a given polynomial.
    /// </summary>
    /// <remarks>
    ///     For ISO 3309 compliant 64-bit CRC's use Crc64Iso.
    /// </remarks>
    public class CRC64 : HashAlgorithm
    {
        public const ulong DefaultSeed = 0x0;

        private readonly ulong _seed;

        private readonly ulong[] _table;
        private ulong _hash;

        public CRC64(ulong polynomial)
            : this(polynomial, DefaultSeed)
        {
        }

        public CRC64(ulong polynomial, ulong seed)
        {
            _table = InitializeTable(polynomial);
            _seed = _hash = seed;
        }

        public override int HashSize
        {
            get { return 64; }
        }

        public override void Initialize()
        {
            _hash = _seed;
        }

        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            _hash = CalculateHash(_hash, _table, array, ibStart, cbSize);
        }

        protected override byte[] HashFinal()
        {
            var hashBuffer = UInt64ToBigEndianBytes(_hash);
            HashValue = hashBuffer;
            return hashBuffer;
        }

        protected static ulong CalculateHash(ulong seed, ulong[] table, IList<byte> buffer, int start, int size)
        {
            var hash = seed;
            for (var i = start; i < start + size; i++)
            {
                unchecked
                {
                    hash = (hash >> 8) ^ table[(buffer[i] ^ hash) & 0xff];
                }
            }
            return hash;
        }

        private static byte[] UInt64ToBigEndianBytes(ulong value)
        {
            var result = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(result);
            }

            return result;
        }

        private static ulong[] InitializeTable(ulong polynomial)
        {
            if (polynomial == CRC64Iso.Iso3309Polynomial && CRC64Iso.Table != null)
            {
                return CRC64Iso.Table;
            }

            var createTable = CreateTable(polynomial);

            if (polynomial == CRC64Iso.Iso3309Polynomial)
            {
                CRC64Iso.Table = createTable;
            }

            return createTable;
        }

        protected static ulong[] CreateTable(ulong polynomial)
        {
            var createTable = new ulong[256];
            for (var i = 0; i < 256; ++i)
            {
                var entry = (ulong) i;
                for (var j = 0; j < 8; ++j)
                {
                    if ((entry & 1) == 1)
                    {
                        entry = (entry >> 1) ^ polynomial;
                    }
                    else
                    {
                        entry = entry >> 1;
                    }
                }
                createTable[i] = entry;
            }
            return createTable;
        }
    }
}